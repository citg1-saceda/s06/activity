-- a. List the books Authored by Marjorie Green.
SELECT TITLE.title FROM AUTHOR
INNER JOIN AUTHOR_TITLE ON AUTHOR_TITLE.au_id = AUTHOR.au_id
INNER JOIN TITLE ON TITLE.title_id = AUTHOR_TITLE.title_id
WHERE AUTHOR.au_fname = 'Marjorie' AND 
AUTHOR.au_lname = 'Green';

-- b. List the books Authored by Michael O'Leary.
SELECT TITLE.title FROM AUTHOR
INNER JOIN AUTHOR_TITLE ON AUTHOR_TITLE.au_id = AUTHOR.au_id
INNER JOIN TITLE ON TITLE.title_id = AUTHOR_TITLE.title_id
WHERE AUTHOR.au_fname = 'Michael' AND 
AUTHOR.au_lname = 'O''Leary';

-- c. Write the author/s of "The Busy Executives Database Guide".
SELECT AUTHOR.au_fname, AUTHOR.au_lname FROM AUTHOR
INNER JOIN AUTHOR_TITLE ON AUTHOR_TITLE.au_id = AUTHOR.au_id
INNER JOIN TITLE ON TITLE.title_id = AUTHOR_TITLE.title_id
WHERE TITLE.title = 'The Busy Executive''s Database Guide';

-- d. Identify the publisher of "But Is It User Friendly?".
SELECT PUBLISHER.pub_name FROM PUBLISHER
INNER JOIN TITLE ON TITLE.pub_id = PUBLISHER.pub_id
WHERE TITLE.title = 'But Is It User Friendly?';

-- e. List the books published by Algodata Infosystems.
SELECT TITLE.title FROM TITLE
INNER JOIN PUBLISHER ON PUBLISHER.pub_id = TITLE.pub_id
WHERE PUBLISHER.pub_name = 'Algodata Infosystems';

-- Create Sql Syntax and Queries to create a database based on the ERD:
-- Database Name: blog_db
CREATE DATABASE blog_db;

-- Use the database created
USE blog_db;

-- Create "users" table
CREATE TABLE users(
id INT NOT NULL AUTO_INCREMENT,
email VARCHAR(100) NOT NULL,
password VARCHAR(300) NOT NULL,
datetime_created DATETIME NOT NULL,
PRIMARY KEY (id)
);

-- Create "posts" table
CREATE TABLE posts(
id INT NOT NULL AUTO_INCREMENT,
author_id INT NOT NULL,
title VARCHAR(500) NOT NULL,
content VARCHAR(5000) NOT NULL,
datetime_posted DATETIME NOT NULL,
PRIMARY KEY (id),
CONSTRAINT fk_users_id_author
    FOREIGN KEY (author_id) REFERENCES users(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);

-- Create "post_likes" table
CREATE TABLE post_likes(
id INT NOT NULL AUTO_INCREMENT,
post_id INT NOT NULL,
user_id INT NOT NULL,
datetime_liked DATETIME NOT NULL,
PRIMARY KEY (id),
CONSTRAINT fk_posts_likes_post_id
    FOREIGN KEY (post_id) REFERENCES posts(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
CONSTRAINT fk_posts_likes_user_id
    FOREIGN KEY (user_id) REFERENCES users(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);

-- Create "post_comments" table
CREATE TABLE post_comments(
id INT NOT NULL AUTO_INCREMENT,
post_id INT NOT NULL,
user_id INT NOT NULL,
datetime_commented DATETIME NOT NULL,
PRIMARY KEY (id),
CONSTRAINT fk_posts_comment_post_id
    FOREIGN KEY (post_id) REFERENCES posts(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
CONSTRAINT fk_posts_comment_user_id
    FOREIGN KEY (user_id) REFERENCES users(id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);

